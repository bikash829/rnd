<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentModel extends CI_Model
{

	public function insert_student($data){
		$status = $this->db->insert('students',$data);
		return $status;

	}
	public function retrieve_students_data(){
		$this->db->from('students');
		$query = $this->db->get();
		return $query->result();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$status = $this->db->delete('students');
		return $status;
	}

	public function update_data($data,$id){
		$this->db->where('id', $id);
		$this->db->update('students', $data);

	}

	public function retrieveData($id){
		$this->db->where('id',$id);
		$query = $this->db->get('students');
		return $query->result();
	}

}
