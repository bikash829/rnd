<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddStudentController extends CI_Controller
{

	public function view_form(){
//		$data = array($this->session->flash_data('msg_success'));
		$this->load->view('dashboard/user');
	}

	public function view_dashboard(){
		$this->load->view('dashboard/dashboard');
	}

	public function view_student_list(){
		$data['students'] = $this->StudentModel->retrieve_students_data();

		$this->load->view('dashboard/tables',$data);
	}

//	public function delete_student(){
//		$uid = $this->input->get('id');
//		$this->load->model('StudentModel');
//		$result = $this->StudentModel->delete($uid);
//		if ($result){
//			$success= "Your data has been deleted successfully";
//
//			redirect('student_list',$success);
//		}else{
//			echo "Sorry there is an technical error, try again";
//		}
//	}

	public function store_data(){
		$f_name = $this->input->post('f_name');
		$l_name = $this->input->post('l_name');
		$name = $f_name.' '.$l_name;
		$email = $this->input->post('email');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$country = $this->input->post('country');
		$post_code = $this->input->post('post_code');
		$st_about = $this->input->post('st_about');

		$data = array("name"=>$name, "email"=>$email, "address"=>$address,"city"=>$city,"country"=>$country,"post_code"=>$post_code,"st_about"=>$st_about);

		$this->load->model('StudentModel');
		$this->StudentModel->insert_student($data);
	}

	//delete record
	public function delete_student(){
		$uid =  $this->input->post('id');

		$this->load->model('StudentModel');
		$result = $this->StudentModel->delete($uid);
		if ($result){
			return true;
		}else{
			echo "Sorry there is an technical error, try again";
		}
	}


	//edit data
//	public function retrieve_data(){
//		$uid = $this->input->post('id');
//		$this->load->model('StudentModel');
//		$this->StudentModel->retrieve_St($uid);
//	}
  //update data
  public function update_data(){
	  $id = $this->input->post('id');
	  $name = $this->input->post('name');
	  $address = $this->input->post('address');
	  $city = $this->input->post('city');
	  $country = $this->input->post('country');
	  $post_code = $this->input->post('post_code');
	  $st_about = $this->input->post('st_about');
	  $data = array("name"=>$name, "address"=>$address,"city"=>$city,"country"=>$country,"post_code"=>$post_code,"st_about"=>$st_about);

	  $this->load->model('StudentModel');
	  $this->StudentModel->update_data($data,$id);
  }


  //PDF info download
	public function downloading_info(){
		$uid = $this->input->get('id');
		$this->load->model('StudentModel');
		$data = $this->StudentModel->retrieveData($uid);


		//mpdf code
		$mpdf = new \Mpdf\Mpdf();
		$html = "<pre>Name : {$data[0]->name}\nEmail Id : {$data[0]->email}\nAddress : {$data[0]->address}, {$data[0]->city}-{$data[0]->post_code}\nCountry : {$data[0]->country}\nShort Description : {$data[0]->st_about}</pre>";
		$file_name = str_replace(' ','_',$data[0]->name).uniqid().time();
		$mpdf->WriteHTML($html);
		$mpdf->Output($file_name,'D');
	}








}
