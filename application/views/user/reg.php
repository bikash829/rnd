<!DOCTYPE html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Join Us</title>
	<link rel="stylesheet" type="text/css" href="asset/css/style.css">
	<link rel="stylesheet" type="text/css" href="asset/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="asset/font-awesome/all.min.css">
</head>
<body class="registration">

<div class="container py-3">
	<div class="row justify-content-center">
		<div class="col-12 col-md-10 col-lg-10">
			<div class="card  cus_card">
				<div class="card-header">
					<h2>Register</h2>
				</div>
				<!--card body start-->
				<div class="card-body px-4 ">
					<!-- Form start -->
					<form id="sign_up_form" action="<?=base_url()?>backend/reg.php" method="POST" enctype="multipart/form-data">
						<!--user name -->
						<div class="form-group row">
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="first_name" class="col-form-label">First Name</label>
								<input type="text" id="first_name" name="first_name" required class="form-control" placeholder="Jhon">
							</div>
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="last_name" class="col-form-label">Last Name</label>
								<input type="text" id="last_name" name="last_name" required class="form-control" placeholder="Doy">
							</div>
						</div>
						<!--user name end-->
						<!--email -->
						<div class="form-group row">
							<div class="custom_input col-12">
								<label for="email_id" class="col-form-label">Email</label>
								<input type="eamil" id="email_id" name="email" required class="form-control" placeholder="ex#jhondoy@gmail.com">
							</div>
						</div>
						<!--email end-->
						<!--password -->
						<div class="form-group row" >
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="password" class="col-form-label">Password</label>
								<input type="password" id="password"  name="pass" required class="form-control">
							</div>
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="con_password" class="col-form-label">Confirm Password</label>
								<input type="password" id="con_password" name="re_pass" required class="form-control">
							</div>
						</div>
						<!--password end-->
						<!--gender & Date of Birth-->
						<div class="from-group row">

							<div class="col-12 col-sm-6 col-md-6 col-lg-6">
								<div>
									<label for="date_of" class="col-form-label">Gender</label>
								</div>

								<div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" required type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
										<label class="form-check-label" for="inlineRadio1">Male</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
										<label class="form-check-label" for="inlineRadio2">Female</label>
									</div>

								</div>
							</div>




							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="date_of" class="col-form-label">Date Of Birth</label>
								<input type="date" id="date_of" name="dob" required class="datepicker form-control">
							</div>
						</div>
						<!--gender & Date of Birth end-->
						<div class="form-group row">
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="country_name" class="col-form-label">Country</label>

								<select class="form-select"  required name="country" aria-label="Default select example">
									<option selected>Open this select menu</option>
									<option value="1">Bangladesh</option>
									<option value="2">France</option>
									<option value="3">Germany</option>
									<option value="4">United States</option>
									<option value="5">United Kingdom</option>
								</select>
							</div>
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="city_name" class="col-form-label">City</label>
								<input type="text" id="city_name" name="city" required class="form-control" placeholder="EX# London">
							</div>
						</div>
						<!--basic info end-->
						<!--Phone and zip code -->
						<div class="form-group row">
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="zip_code_id" class="col-form-label">Zip Code</label>
								<input type="text" id="zip_code_id" name="zip_code" required class="form-control" placeholder="ex# 1200">
							</div>
							<div class="custom_input col-12 col-sm-6 col-md-6 col-lg-6">
								<label for="phone_code" class="col-form-label">Phone Number </label>
								<div class="">
									<div class="custom_input">
										<input id="phone_code" type="number" name="phone" required placeholder="1800XXXXXX" class="form-control">
									</div>
								</div>
							</div>

						</div>
						<!--hone and zip code  end-->

						<!--Checkbox -->
						<div class="form-group">
							<div class="custom-control custom_input custom-checkbox mb-3">
								<input type="checkbox" name="read_and_accept" class="custom-control-input" id="customControlValidation1" required>
								<label class="custom-control-label" for="customControlValidation1">Check this custom checkbox</label>
								<div class="invalid-feedback">Example invalid feedback text</div>
							</div>
						</div>
						<!--Checkbox end-->
					</form>
					<!--Form end-->
				</div>
				<!--card body end-->
				<div class="card-footer px-4">

					<div class="d-grid gap-2 col-6 mx-auto">
						<button form="sign_up_form" class="btn btn-lg btn-primary" type="submit" name="btn_sign_up">Confirm Registration</button>
					</div>

					<div class="d-flex justify-content-center pb-3">
						<a class="sign_text"  href="<?=base_url()?>login">Already Have an account</a>
					</div>


				</div>
			</div>

		</div>
	</div>
</div>



<script src="asset/bootstrap/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="asset/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="asset/font-awesome/all.min.js" type="text/javascript"></script>
</body>
</html>
